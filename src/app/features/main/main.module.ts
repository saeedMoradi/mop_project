import { NgModule } from "@angular/core";
import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import { WatchListComponent } from './watch-list/watch-list.component';
import {MatTableModule} from '@angular/material/table';
import { CommonModule } from '@angular/common';

@NgModule({
    declarations: [
      MainComponent,
      WatchListComponent
    ],
    imports: [
        MainRoutingModule,
        MatTableModule,
        CommonModule
    ],
    exports:[],
})

export class MainModule { }