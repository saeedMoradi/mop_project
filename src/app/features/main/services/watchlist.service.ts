import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { 

} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class WatchlistService {
  private readonly apiBaseUrl = environment.base_url;
  constructor(private http: HttpClient) { }

  allWatchlistColumns(): Observable<any> {
    return this.http.get<any>(`${this.apiBaseUrl}/watch/columns/`);
  }
}
