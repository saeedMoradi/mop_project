import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
// Module
import { ShardModule } from '../../shared/shared.module';
import { MessagesRoutingModule } from "./messages-router.module";
import {DpDatePickerModule} from 'ng2-jalali-date-picker';
import { MaterialModule } from 'src/app/shared/material.module';
// Components
import { SurveilanceComponent } from "./surveilance/surveilance.component";
import { SystemComponent } from "./system/system.component";
import { MessagesComponent } from "./messages.component";

@NgModule({
    declarations:[
        MessagesComponent,
        SurveilanceComponent,
        SystemComponent
  ],
    imports:[
      CommonModule,
      HttpClientModule,
      ShardModule,
      MaterialModule,
      MessagesRoutingModule,
      RouterModule,
      DpDatePickerModule
    ],
})
export class messagesModule{}