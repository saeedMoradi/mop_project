import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder , FormControl } from '@angular/forms';
//services
import { MessagesPageService } from "../services/messages-page.service";
import { PersianCalenderService } from "../../home/services/date/persian-calndar"; 
import { DataService } from "../../home/services/seenMessage";
@Component({
  selector: 'MOP-Login-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.scss']
})
export class SystemComponent implements OnInit {

  constructor(
    private messagesService : MessagesPageService,
    private ref: ChangeDetectorRef,
    private persianCalendarSrvice : PersianCalenderService,
    private fb : FormBuilder,
    private dataService : DataService
  ) { }
  displayedColumns: string[] = [
    'id',
    'title',
    'date',
    'body',
    'icon',
  ];
  public id;
  ngOnInit(): void {
    this.dataService.currentId.subscribe(
      id => {
        this.id = id;
    });
  this.system(null , null , null , null , this.currentPage);
  this.filtersForm = this.fb.group({
    from : new FormControl('' , [
      Validators.required,
      Validators.min(1),
    ]),
    until : [ '' , [
      Validators.required,
      Validators.min(1),
    ]],
  })
  this.searchForm = this.fb.group({
    title : new FormControl('' , [
    ]),
    body : [ '' , [
    ]],
  })
  }
  get from(){
    return this.filtersForm.get('from').value;
  }
  get until(){
    return this.filtersForm.get('until').value;
  }
  get title(){
    return this.searchForm.get('title').value;
  }
  get body(){
    return this.searchForm.get('body').value;
  }

  filtersForm : FormGroup;
  searchForm : FormGroup;
  public survillanceMessages;
  public currentPage = 1;
  public lastPage = 1;
  public showSearch : boolean = false;

  system(from , until , title , body , page){
    this.messagesService.getAllSystemMessages(from , until , title , body , page).subscribe({
      next: (res) => {
        console.log(res);
        this.survillanceMessages = res['results'];
        this.survillanceMessages.forEach(element => {
          element.showMessage = false;
        });
        this.lastPage = res['lastpage'];
        this.ref.detectChanges();
      },
      error : (err) => {
        console.log(err);
      }
    })
  }

  seenMessage(element){
    // console.log(id);
    this.messagesService.deleteUnseen(element.id).subscribe(res => {
      console.log(res);
    })
    this.dataService.changeId(element.id);
    if(element.showMessage == true){
      this.system(this.miladiDate(this.from) , this.miladiDate(this.until) , this.title , this.body , this.currentPage);
    }
  }
  miladiDate(date){
    if(date){
      // console.log(date);
    var jalaali = require('jalaali-js');
    var dd = date.split("-");
    let year = dd[0];
    let month = dd[1];
    let day = dd[2];
    // console.log();
  if(year == 9999){
    return 'inf';
  }
  else{
    let shamsi =  jalaali.toGregorian(Number(year), Number(month), Number(day));
    console.log(shamsi.gd + '/' + shamsi.gm + '/' + shamsi.gy);
  return (shamsi.gy + '-' + shamsi.gm + '-' + shamsi.gd)
  } 
    }else{
      return null;
    }
   
  }
  filter(){
    this.system(this.miladiDate(this.from) , this.miladiDate(this.until) , this.title , this.body , 1);
  }
search(){
  // console.log(this.title.value , this.body.value);
  this.system(this.miladiDate(this.from) , this.miladiDate(this.until) , this.title , this.body , 1);
}
  reset(){
    this.searchForm.reset();
    this.filtersForm.reset();
    this.system(null , null , null , null , this.currentPage);
  }
  persianDate(element){
    const date = new Date(element);
    return (this.persianCalendarSrvice.PersianCalendar3(date));
  }
nextPage(){
  if(this.currentPage !== this.lastPage){
    this.currentPage = this.currentPage+1;
    // console.log(this.currentPage);
    this.ref.detectChanges();
    this.system(this.miladiDate(this.from) , this.miladiDate(this.until) , this.title , this.body , this.currentPage);
  }
}
previousPage(){
  if(this.currentPage !== 1){
    this.currentPage = this.currentPage-1;
    // console.log(this.currentPage);
    this.ref.detectChanges();
    this.system(this.miladiDate(this.from) , this.miladiDate(this.until) , this.title , this.body , this.currentPage);
  }
}
goToLastPage(){
  this.currentPage = this.lastPage ; 
  this.system(this.miladiDate(this.from) , this.miladiDate(this.until) , this.title , this.body , this.currentPage);
}
goTofirstPage(){
  this.currentPage = 1;
  this.system(this.miladiDate(this.from) , this.miladiDate(this.until) , this.title , this.body , this.currentPage);
}
}
