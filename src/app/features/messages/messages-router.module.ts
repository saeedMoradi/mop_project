import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MessagesComponent } from './messages.component';
import { SurveilanceComponent } from "./surveilance/surveilance.component";
import { SystemComponent } from "./system/system.component";

const routes: Routes = [
  { path: '', component: MessagesComponent,
  children:[
    {
    path:'',
    redirectTo : 'surveillance',
    },
    {
      path : 'system',
      component : SystemComponent
    },
    {
      path : 'surveillance',
      component : SurveilanceComponent,
    }
  ]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MessagesRoutingModule { }
