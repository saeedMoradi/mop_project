import { TestBed } from '@angular/core/testing';

import { MessagesPageService } from './messages-page.service';

describe('MessagesPageService', () => {
  let service: MessagesPageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MessagesPageService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
