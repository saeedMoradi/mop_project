import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import { BehaviorSubject, Subject } from "rxjs";
import { 
  response_getAllsurveillanceMessages,
  response_getAllSystemMessages
} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class MessagesPageService {
  private readonly apiBaseUrl = environment.base_url;
  constructor(private http: HttpClient) { }

  getAllSurveillanceMessages(from , until , title , body , page): Observable<response_getAllsurveillanceMessages> {
    console.log(`${this.apiBaseUrl}/messages/surveillance/?from=${from}&until=${until}&title=${title}&body=${body}&page=${page}`);
    return this.http.get<response_getAllsurveillanceMessages>(`${this.apiBaseUrl}/messages/surveillance/?from=${from}&until=${until}&title=${title}&body=${body}&page=${page}`);
  }
  getAllSystemMessages(from , until , title , body , page): Observable<response_getAllSystemMessages> {
    console.log(`${this.apiBaseUrl}/messages/system/?from=${from}&until=${until}&title=${title}&body=${body}&page=${page}`);
    return this.http.get<response_getAllSystemMessages>(`${this.apiBaseUrl}/messages/system/?from=${from}&until=${until}&title=${title}&body=${body}&page=${page}`);
  }
  deleteUnseen (id): Observable<any> {
    // console.log(`${this.apiBaseUrl}/messages/unseen/${id}/`);
    return this.http.delete<any>(`${this.apiBaseUrl}/messages/unseen/${id}/`)
  }
}
