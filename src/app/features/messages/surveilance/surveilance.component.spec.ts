import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SurveilanceComponent } from './surveilance.component';

describe('SurveilanceComponent', () => {
  let component: SurveilanceComponent;
  let fixture: ComponentFixture<SurveilanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SurveilanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SurveilanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
