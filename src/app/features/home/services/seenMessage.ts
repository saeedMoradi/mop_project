import {Injectable , EventEmitter} from "@angular/core";    
import { Observable, Subject , BehaviorSubject  } from 'rxjs';

@Injectable()
export class DataService {
    private node$ =  new BehaviorSubject ("default id");
    currentId = this.node$.asObservable(); 

    constructor(){}

    changeId(id){
        this.node$.next(id);
        // console.log(this.currentId);
    }
}