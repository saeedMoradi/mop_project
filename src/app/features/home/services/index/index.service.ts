import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { 
 response_getIndex ,
 request_postFavoriteIndex
} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class IndexService {
  private readonly apiBaseUrl = environment.base_url;

  constructor(private http: HttpClient) { }

  getIndex(): Observable<response_getIndex> {
    return this.http.get<response_getIndex>(`${this.apiBaseUrl}/index/`);
  }
  getFavoriteIndex(): Observable<any> {
    return this.http.get(`${this.apiBaseUrl}/index/favorite/`);
  }
  postFavoriteIndex(data: request_postFavoriteIndex): Observable<any> {
    return this.http.put<any>(`${this.apiBaseUrl}/index/favorite/`, data);
  }

}
