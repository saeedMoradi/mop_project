import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { 
  response_getDate
} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DateService {

  private readonly apiBaseUrl = environment.base_url;

  constructor(private http: HttpClient) { }

  getDate(): Observable<response_getDate> {
    return this.http.get<response_getDate>(`${this.apiBaseUrl}/datetime/`);
  }
}
