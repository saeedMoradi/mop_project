import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { 
  response_getAccountingInfo,
  response_getAccounting,
  response_getWithdraw
} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class AccountingService {
  private readonly apiBaseUrl = environment.base_url;

  constructor(private http: HttpClient) { }

  getAccountingInfo(): Observable<response_getAccountingInfo> {
    return this.http.get<response_getAccountingInfo>(`${this.apiBaseUrl}/accounting/info/`);
  }
  getAccounting(): Observable<response_getAccounting> {
    return this.http.get<response_getAccounting>(`${this.apiBaseUrl}/accounting/account/`);
  }
  getWithdraw(): Observable<response_getWithdraw> {
    return this.http.get<response_getWithdraw>(`${this.apiBaseUrl}/accounting/withdraw/`);
  }
  postWithdraw(data: any): Observable<any> {
    return this.http.post<any>(`${this.apiBaseUrl}/accounting/withdraw/`, data);
  }
}
