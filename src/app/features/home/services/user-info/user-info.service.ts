import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { 
response_getUserInfo
} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class UserInfoService {

  private readonly apiBaseUrl = environment.base_url;

  constructor(private http: HttpClient) { }

  getUserInfo(): Observable<response_getUserInfo> {
    return this.http.get<response_getUserInfo>(`${this.apiBaseUrl}/user/info/`);
  }
}
