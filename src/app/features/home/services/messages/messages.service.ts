import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
// import { BehaviorSubject, Subject } from "rxjs";
import { 
  response_UnseenMessages,
  response_surveillanseMessages,
  response_NotifMSG
} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class MessagesService {

  private readonly apiBaseUrl = environment.base_url;
  // private messageSource = new BehaviorSubject<any[]>([]);
  // currentMessage = this.messageSource.asObservable();


  constructor(private http: HttpClient) { }

  getUnseenMessages(): Observable<response_UnseenMessages> {
    return this.http.get<response_UnseenMessages>(`${this.apiBaseUrl}/messages/unseen/`);
  }
  getSurveillanceMessages(): Observable<response_surveillanseMessages> {
    return this.http.get<response_surveillanseMessages>(`${this.apiBaseUrl}/messages/today/surveillance/`);
  }
  getSystemMessages(): Observable<response_UnseenMessages> {
    return this.http.get<response_UnseenMessages>(`${this.apiBaseUrl}/messages/today/system/`);
  }
  deleteUnseen (id): Observable<any> {
    // console.log(`${this.apiBaseUrl}/messages/unseen/${id}/`);
    return this.http.delete<any>(`${this.apiBaseUrl}/messages/unseen/${id}/`)
  }
  seenAll() : Observable<any> {
    return this.http.delete<any>(`${this.apiBaseUrl}/messages/unseen/all/`)
  }

  // showMessage(data) {
  //   this.messageSource.next(data);
  // }

  // removeMessage(data) {
  //   const message: any[] = [...this.messageSource.getValue()];
  //   message.forEach((item, index) => {
  //     if (item === data) {
  //       message.splice(index, 1)
  //     }
  //   });
  //   this.messageSource.next(message);
  // }

  notifMessages(): Observable<response_NotifMSG> {
    return this.http.get<response_NotifMSG>(`${this.apiBaseUrl}/messages/priority/`);
  }
  
}
