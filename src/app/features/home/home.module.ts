import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
// Module
import { HomeRouterModule } from './home-router.module';
import { MatIconModule } from '@angular/material/icon';
import { MaterialModule } from 'src/app/shared/material.module';

// Components
import { HomeComponent } from './home.component';
import { FooterComponent } from './footer/footer.component';
import { NavbarComponent } from './navbar/navbar.component';
import { MessagesSideComponent } from './navbar/messages-side/messages-side/messages-side.component';
import { DialogComponent } from './navbar/dialog/dialog.component';
import { ShowMessageComponent } from './navbar/messages-side/show-message/show-message.component';
import { NotifSystemComponent } from './navbar/notif-system/notif-system.component';
import { DataService } from "../home/services/seenMessage";

@NgModule({
    declarations:[
    HomeComponent,
    FooterComponent,
    NavbarComponent,
    MessagesSideComponent,
    DialogComponent,
    ShowMessageComponent,
    NotifSystemComponent
  ],
    imports:[
      CommonModule,
      HttpClientModule,
      FormsModule, ReactiveFormsModule,
      HomeRouterModule,
      MatIconModule,
      MaterialModule
    ],
  
      // MatDialogModule
    // ]
    providers:[
      DataService
    ]
})

export class HomeModule {}