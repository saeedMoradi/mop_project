import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from "./home.component";

const routes: Routes = [
    { path: '', component: HomeComponent, children:[
        {path: '', redirectTo: 'main',pathMatch: 'full'},
        {path: 'main', loadChildren: () => import('../main/main.module').then(m => m.MainModule)},
        {
            path: 'messages',
            loadChildren: () =>
              import('../messages/messages.module').then(m => m.messagesModule)
        },
    ] },
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class HomeRouterModule {}