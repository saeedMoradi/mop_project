import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ShowMessageData } from 'src/app/shared/interfaces';
import { PersianCalenderService } from "../../../services/date/persian-calndar";

@Component({
  selector: 'MOP-Login-show-message',
  templateUrl: './show-message.component.html',
  styleUrls: ['./show-message.component.scss']
})
export class ShowMessageComponent {

  constructor(
    public dialogRef: MatDialogRef<ShowMessageComponent>,
    @Inject(MAT_DIALOG_DATA) public data: ShowMessageData,
    private persianCalendar : PersianCalenderService,
    ) { }

   persianDate(element){
    const date = new Date(element);
    return (this.persianCalendar.PersianCalendar3(date));
  }

  getTime(element){
    const date = new Date(element);
    const time = (date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
    return time;
  }

}
