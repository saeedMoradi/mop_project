import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesSideComponent } from './messages-side.component';

describe('MessagesSideComponent', () => {
  let component: MessagesSideComponent;
  let fixture: ComponentFixture<MessagesSideComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MessagesSideComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesSideComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
