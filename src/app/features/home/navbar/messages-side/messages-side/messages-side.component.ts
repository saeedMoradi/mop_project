import { Component, OnInit , Input , EventEmitter,OnChanges , SimpleChanges, Output } from '@angular/core';
import { MessagesService } from "../../../services/messages/messages.service";
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ShowMessageComponent } from '../show-message/show-message.component';
import { ShowMessageData } from '../../../../../shared/interfaces/showMessageData.interface';
import { PersianCalenderService } from "../../../services/date/persian-calndar";

@Component({
  selector: 'MOP-messages-side',
  templateUrl: './messages-side.component.html',
  styleUrls: ['../../navbar.component.scss']
})
export class MessagesSideComponent implements OnInit {

  @Input() openMessages: boolean;
  @Output() getUnseen = new EventEmitter <string> ();
  messages: ShowMessageData;
  messageFilterSystem = [];
  messageFilterSurvill = [];
  showFilterMSG: boolean = false;
  showFilterSystem: boolean = false;

  constructor( 
    private messagesService:MessagesService, 
    private dialog: MatDialog,
    private persianCalendar : PersianCalenderService
     ) { }
  public selectedHEader = 1;
  public survillanceMessages;
  public systemMessages;
  surveillance(){
    this.messagesService.getSurveillanceMessages().subscribe({
      next: (res) => {
        console.log(res);
        this.survillanceMessages = res;
      },
      error : (err) => {
        console.log(err);
      }
    })
  }

  system(){
    this.messagesService.getSystemMessages().subscribe({
      next: (res) => {
        console.log(res);
        this.systemMessages = res;
      },
      error : (err) => {
        console.log(err);
      }
    })
  }

  onSearchChange(val: string) {
    this.messageFilterSystem = [];
    this.messageFilterSurvill = [];
    console.log(this.selectedHEader, 'search val')

    if (this.selectedHEader == 1) {
      this.survillanceMessages.filter(msg => {
        if (msg.title.indexOf(val) > -1 || msg.body.indexOf(val) > -1) {
          this.messageFilterSurvill.push(msg);
          // console.log(this.messageFilterSurvill)
          this.showFilterMSG = true;
        }
      })
    }else if(this.selectedHEader == 2) {
      // console.log('this is a test')
      this.systemMessages.filter(msg => {
        if (msg.title.indexOf(val) > -1 || msg.body.indexOf(val) > -1) {
          this.messageFilterSystem.push(msg);
          // console.log(this.messageFilterSystem, 'survaill')
          this.showFilterSystem = true;
        }
      })
    }
  }

  reloadSearch(val: string) {
    this.onSearchChange(val)
  }

  persianDate(element){
    const date = new Date(element);
    return (this.persianCalendar.PersianCalendar3(date));
  }

  getTime(element){
    const date = new Date(element);
    const time = (date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
    return time;
  }

  cutText(text){
    return text.substr(0,20);
  }

  ngOnInit(): void {
    this.surveillance();
    this.system();
  }
  ngOnChanges(changes: SimpleChanges): void{
    if(changes){
      if(changes['openMessages']['currentValue'] == true){
        this.selectedHEader = 1;
      }
    }
  }

  unseenMSG(data) {
    this.messagesService.deleteUnseen(data.id).subscribe(res => {
      console.log(res);
      this.surveillance();
      this.system();
      this.getUnseen.emit('get');
    })
    let messages = [];
    messages.push(data)

    const dialogRef = this.dialog.open(ShowMessageComponent, {
      width: '40vw',
      height: 'auto',
      data: messages
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result);
    });
  }

  seenAll(){
    this.messagesService.seenAll().subscribe(res => {
      console.log(res);
      this.surveillance();
      this.system();
      this.getUnseen.emit('get');
    })
  }

}
