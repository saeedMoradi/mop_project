import { Component, Inject, OnInit , ChangeDetectorRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, Validators, FormBuilder , FormControl } from '@angular/forms';
//services
import { AccountingService } from "../../services/accounting/accounting.service";


@Component({
  selector: 'MOP-Login-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {
  withdrawForm : FormGroup;
  showItem = 'harvest';
  displayBox: boolean = false;
  public banks;
  public selectedBank;
  public dates;
  public selectedDate;

  constructor(public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private accountingService : AccountingService,
    private fb : FormBuilder,
    private ref: ChangeDetectorRef,
    ) { }

  ngOnInit(): void {
    this.getAccountingInfo();
    this.getWithdraw();

    this.withdrawForm = this.fb.group({
      amount : new FormControl('' , [
        Validators.required,
        Validators.min(1),
      ]),
      bank : [ '' , [
        Validators.required,
      ]],
      date : [ '' , [
        Validators.required,
      ]],
      comment : [ '' , []],
    })
  } 

  get amount(){
    return this.withdrawForm.get('amount');
  }
  get bank(){
   return this.withdrawForm.get('bank');
 }
 get date(){
   return this.withdrawForm.get('date');
 }
 get comment(){
   return this.withdrawForm.get('comment');
 }

  getAccountingInfo(){
    this.accountingService.getAccounting().subscribe(res => {
      console.log(res);
      this.banks = res;
    },
  )
  }
  getWithdraw(){
    this.accountingService.getWithdraw().subscribe(res => {
      console.log(res);
      this.dates = res;
    },
  )
  }
  withdraw(){
    console.log(this.bank.value , this.date.value , this.amount.value);
    this.accountingService.postWithdraw({ "date" : this.date.value , "amount" : this.amount.value , "account_id" : this.bank.value , 
    "comment": this.comment.value,
  }).subscribe(
      res =>{
        console.log(res);
        this.dialogRef.close();
      }
    )
  }

  save() {
    this.dialogRef.close('it was closed')
  }

  showBox(box) {
    if (box == 'harvest') {
      this.displayBox = true;
      this.showItem = 'harvest'
    } else if(box == 'deposit') {
      this.displayBox = false;
      this.showItem = 'deposit'
    }
  }

}
