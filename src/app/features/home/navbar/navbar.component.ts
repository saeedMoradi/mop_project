import { Component, OnInit , HostListener , ElementRef , ViewChild , ChangeDetectorRef } from '@angular/core';
import { Router } from "@angular/router";
//services
import { PersianCalenderService } from "../services/date/persian-calndar";
import { DateService } from "../services/date/date.service";
import { MessagesService } from "../services/messages/messages.service";
import { IndexService } from "../services/index/index.service";
import {MatDialog, MatDialogConfig, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { DialogComponent } from './dialog/dialog.component'
import { UserInfoService } from "../services/user-info/user-info.service";
import { AccountingService } from "../services/accounting/accounting.service";
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { AuthService } from '../../auth/services/auth.service';
import { DataService } from "../../home/services/seenMessage";
import { Subscription } from 'rxjs';

@Component({
  selector: 'MOP-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  @ViewChild("sideMenu") sideMenu : ElementRef;
  subscription: Subscription;
  constructor(
    private persianCalendar : PersianCalenderService , 
    private elementRef: ElementRef , 
    private dateService : DateService,
    private ref: ChangeDetectorRef,
    private messagesService : MessagesService,
    private indexService : IndexService,
    private dialog: MatDialog,
    private userInfoService : UserInfoService,
    private accountingService : AccountingService,
    private persistanceService: PersistanceService,
    public router : Router, 
    private authService: AuthService,
    private dataService : DataService
    ) {
      this.updateId();
    }
  public farsiDate;
  public showSide : boolean = false;
  public today = new Date();
  public openMessages : boolean = false;
  public year;
  public month;
  public day;
  public hour;
  public minute;
  public second;
  public unseenCount = 0;
  public faveName = ''; 
  public favePercentage;
  public faveValue;
  public faveMarketValue;
  public faveId;
  public usersName;
  public usersCode;
  public allIndexes;
  public balance;
  public available;
  public blocked;
  public t1;
  public t2;
  public showAccountingSubMenu : boolean = false;
  
  getUnseen(event){
    console.log(event);
    this.unseenMessages();
  }


  getDate(){
    this.dateService.getDate().subscribe(res => {
        // console.log(res);
        this.hour = res['hour'];
        this.minute = res['minute'];
        this.second = res['second'];
        this.year = res['year'];
        this.month = res['month']-1;
        this.day = res['day'];
        this.today = new Date( this.year, this.month , this.day , this.hour  , this.minute, this.second);  
        this.getJalaliDate(this.today);
        this.incrementClock();
        this.ref.detectChanges();
      },)
  }
  unseenMessages(){
    this.messagesService.getUnseenMessages().subscribe(res => {
        console.log(res);
        this.unseenCount = res['count'];
        this.ref.detectChanges();
      },
    )
  }

  userInfo(){
    this.userInfoService.getUserInfo().subscribe(res => {
        console.log(res);
        this.usersName = res['name'];
        this.usersCode = res['trade_code'];
        this.ref.detectChanges();
      },
    )
  }

  accountingInfo(){
    this.accountingService.getAccountingInfo().subscribe(res => {
        console.log(res);
        this.balance = res['balance'];
        this.available = res['available'];
        this.blocked = res['blocked'];
        this.t1 = res['t1'];
        this.t2 = res['t2'];
        this.ref.detectChanges();
      },
    )
  }

  index(){
    this.indexService.getIndex().subscribe(res => {
        console.log(res);
        this.allIndexes = res;
        this.ref.detectChanges();
      },
    )
  }
  favoriteIndex(){
    this.indexService.getFavoriteIndex().subscribe(res => {
        console.log(res);
        this.faveName = res['name']; 
        this.favePercentage = res['change_percentage'];
        this.faveValue = res['value'];
        this.faveMarketValue = res['trade_value'];
        this.faveId = res['ins_id'];
        this.ref.detectChanges();
      },
    )
  }
  makeFavorite(id){
    this.indexService.postFavoriteIndex({ "ins_id" :  id}).subscribe (res =>{
      console.log(res);
      this.favoriteIndex();
      this.ref.detectChanges();
    }
    )
  }
  incrementClock(){
    if(this.second){
     this.second = this.second+1;
     this.ref.detectChanges();
    if (this.second == 60){
        this.second=0;
        this.minute = this.minute+1;
        this.getDate();
       this.incrementClock();
    }
    if (this.minute == 60){
        this.minute=0;
        this.hour = this.hour + 1;
        this.ref.detectChanges();
    }
    this.today = new Date( this.year, this.month , this.day , this.hour  , this.minute, this.second); 
    }
    this.ref.detectChanges();
  }
  getJalaliDate(date) {
    var date1 = this.persianCalendar.PersianCalendar(date);
    this.farsiDate = date1;
  }
  @HostListener('document:mousedown', ['$event'])
  onClick(event: MouseEvent): void {
    if (!this.elementRef.nativeElement.contains(event.target)) {
      this.showSide = false;
    }
  }
  public id;
  updateId(){
    this.dataService.currentId.subscribe(
      res => {
        // console.log(res);
        this.unseenMessages();
        this.id = res;
    }
    );
  }


  ngOnInit(): void {
    setInterval(()=>{
      let refreshTok = {
        'refresh_token' : this.persistanceService.get('refresh_token')
      }
      this.authService.postRefresh(refreshTok).subscribe(res => {
        res.access_token
        console.log('test token');
        
        this.persistanceService.set('access_token', res.access_token);
        this.persistanceService.set('lifetime', res.lifetime);
      });
    },this.persistanceService.get('lifetime') * 990)

    this.getDate();
    this.unseenMessages();
    this.index();
    this.favoriteIndex();
    this.userInfo();
    this.accountingInfo();

    setInterval(() => {
      this.incrementClock();
    }, 1000);
    setInterval(() => {
      this.getDate()
    }, 1000);
  }

  openDialog() {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '40vw',
      height: 'auto',
      minHeight : '500px'
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.accountingInfo();
      
      console.log(result);
    });
  }

  logOut(){
    // this.persistanceService.remove('refresh_token');
    // this.persistanceService.remove('access_token');
    this.persistanceService.removeAll()
    this.router.navigateByUrl('login');
  }
}
