import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotifSystemComponent } from './notif-system.component';

describe('NotifSystemComponent', () => {
  let component: NotifSystemComponent;
  let fixture: ComponentFixture<NotifSystemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotifSystemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotifSystemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
