import { Component, OnInit , Output , EventEmitter } from '@angular/core';
import { MessagesService } from '../../services/messages/messages.service'
import { PersianCalenderService } from "../../services/date/persian-calndar";

@Component({
  selector: 'MOP-notif-system',
  templateUrl: './notif-system.component.html',
  styleUrls: ['./notif-system.component.scss']
})
export class NotifSystemComponent implements OnInit {
  @Output() getUnseen = new EventEmitter <string> ();
  showNotif1: boolean = true;
  showNotif2: boolean = true;
  showNotifAll: boolean = true;
  checked = false;

  // notifSystem: Array<Object> = []
  notifSystem;


  constructor(private messagesService: MessagesService, private persianCalendar : PersianCalenderService) { }

  ngOnInit(): void {
    this.messagesService.notifMessages().subscribe(res => {
      this.notifSystem = res
      console.log(this.notifSystem, 'notif messages')
      if (this.notifSystem.length === 0) {
        this.showNotifAll = false;
      }
    })

  }

  displayBox(msg, checked){
    // this.showNotif = false
    console.log(msg, this.notifSystem)
    for (let i = 0; i < this.notifSystem.length; i++) {
      if (this.notifSystem[i]['id'] === msg.id) {
        this.notifSystem.splice(i,1)
        // this.notifSystem.length === 0 ? this.showNotifAll === false : ''
        if (this.notifSystem.length === 0) {
          this.showNotifAll = false
        }
        if (checked === false) {
          this.messagesService.deleteUnseen(msg['id']) .subscribe(res =>{
            console.log(res);
            this.getUnseen.emit('get');
          })
        }
        if (this.checked == true) {
          this.checked = false
        }
      }
    }
  }

  persianDate(element){
    const date = new Date(element);
    return (this.persianCalendar.PersianCalendar3(date));
  }

  getTime(element){
    const date = new Date(element);
    const time = (date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds());
    return time;
  }

}


