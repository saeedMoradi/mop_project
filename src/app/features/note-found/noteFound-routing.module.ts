import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
import { NoteFoundComponent } from "./note-found.component";

const routes: Routes = [
    { path: '', component: NoteFoundComponent },
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class NoteFoundRoutingModule {}