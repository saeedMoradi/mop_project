import { NgModule } from "@angular/core";
import { NoteFoundComponent } from './note-found.component';
import { NoteFoundRoutingModule } from './noteFound-routing.module';

@NgModule({
    declarations: [
        NoteFoundComponent
    ],
    imports: [NoteFoundRoutingModule]
})

export class NoteFoundModule {}