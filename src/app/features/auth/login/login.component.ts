import { Component, OnInit, ChangeDetectionStrategy ,ViewEncapsulation , ViewChild , ElementRef , ChangeDetectorRef } from '@angular/core';
import { FormGroup, Validators, FormBuilder , FormControl } from '@angular/forms';
import { Router } from "@angular/router";
// service
import { AuthService } from '../services/auth.service';
import { PersistanceService } from 'src/app/core/services/persistance.service';


@Component({
  selector: 'MOP-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  @ViewChild("usernameInput") usernameInput : ElementRef;
  @ViewChild("captchaInput") captchaInput : ElementRef;
  @ViewChild("passwordInput") passwordInput : ElementRef;
  loginForm : FormGroup;
  public hide = true;
  public loginMethod = 'pass';
  public captchaUrl = '';
  public captchaKey;
  public error = '';
  public showKeyboard = false;
  public showKeyboardPass = false;
  public getCode = false;
  private otp_key;
  private otp_code;
  public value = '';
  public showNotifErorr: boolean = true;
  public selectedItem ;
  public imageUrl;
  public dataUsernameInput: string = '';
  constructor(
    private fb : FormBuilder,
    private authService : AuthService,
    public router : Router,
    private ref: ChangeDetectorRef,
    private persistanceService: PersistanceService,
  ) {}
  

  blurUsername(e: any){
    let userVal = e.target.value;
    this.loginForm.get('username').setValue(userVal)
    // console.log(this.loginForm.controls['username'].value, 'blur')
  }

  blurPassword(e: any) {
    let passVal = e.target.value;
    this.loginForm.get('password').setValue(passVal)
    // console.log(this.loginForm.controls['password'].value, 'blur')
  }


  ngOnInit() {
    this.getCaptcha();
    this.loginForm = this.fb.group({
      username : new FormControl('' , [
        Validators.required,
        Validators.min(1),
      ]),
      password : [ '' , [
        Validators.required,
        Validators.min(1),
      ]],
      capcha : [ '' , [
        Validators.required,
        Validators.min(1),
      ]]
    })
  }
  
  getCaptcha(){
    this.authService.postCaptcha('').subscribe(res => {
      // console.log(res);
      this.captchaUrl = res['captcha_image'];
      this.captchaKey = res['captcha_key'];
      this.imageUrl = `data:image/png;base64,${res['captcha_image']}`;
      this.ref.detectChanges();
    });
  }

  loginPass(){
    this.loginForm.markAllAsTouched();
    if(this.loginForm.valid){
      console.log(this.loginForm.value)
    this.authService.postLoginCredentials({
      "captcha_key": this.captchaKey , "captcha_value" : this.capcha.value , "username" : this.username.value , "password" : this.password.value
    }).subscribe(res => {
      this.showNotifErorr = !this.showNotifErorr;
        if (this.showNotifErorr == false) {
          this.error = '';
          this.ref.detectChanges();
        }
        console.log(res, 'login Credentials');
        if(res['opt_type']==0){
          this.router.navigateByUrl('');
          let token = res['access_token'];
          this.persistanceService.set('refresh_token', res.refresh_token);
          this.persistanceService.set('access_token', token)
          this.persistanceService.set('lifetime', res['lifetime'])
          // localStorage.setItem('access_token' , token); 
        }else if(res['opt_type']== 1){
          this.getCode = true;
          this.loginForm.controls['username'].disable();
          this.loginForm.controls['password'].disable();
          this.loginForm.controls['capcha'].disable();
          this.otp_key = res['otp_key'];
        }
    })

    setTimeout(() => {
      if (this.showNotifErorr) {
        this.error = this.persistanceService.get('errorCode');
        this.getCaptcha();
        this.loginForm.controls.capcha.reset();
        if(this.error == '0'){
          this.captchaInput.nativeElement.focus();
        }
        console.log(this.error);
        this.ref.detectChanges();
      }
    }, 1000);

    }else{
      if(this.username.invalid){
        this.usernameInput.nativeElement.focus();
      }
      else if(this.username.valid && this.password.invalid){
        this.passwordInput.nativeElement.focus();
       }
      else if(this.username.valid && this.password.valid && this.capcha.invalid){
        this.captchaInput.nativeElement.focus();
      }
    }
  }


  loginCode(){
    this.authService.postOTP({
      "otp_key" : this.otp_key , "otp_code" : this.otp_code,
     }).subscribe(res => {
      this.showNotifErorr = !this.showNotifErorr;
      if (this.showNotifErorr == false) {
        this.error = '';
        this.ref.detectChanges();
      }
      console.log(res, 'login');
      let token = res['access_token'];
      this.persistanceService.set('access_token', token)
    });
    setTimeout(() => {
      if (this.showNotifErorr || this.persistanceService.get('errorCode')) {
        this.error = this.persistanceService.get('errorCode');
        this.ref.detectChanges();
      }
    }, 1000);
  }

  get username(){
    return this.loginForm.get('username');
  }
  get password(){
   return this.loginForm.get('password');
 }
 get capcha(){
   return this.loginForm.get('capcha');
 }

}
