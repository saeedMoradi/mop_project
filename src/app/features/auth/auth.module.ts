import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
// module
import { ShardModule } from 'src/app/shared/shared.module';
import { AuthRouter } from './auth-router.module';
import { MaterialModule } from '../../shared/material.module';
import { HttpClientModule , HTTP_INTERCEPTORS } from "@angular/common/http";

// components
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotPassComponent } from './forgot-pass/forgot-pass.component';
import { ChangePasswordComponent } from "./change-password/change-password.component";
// interceptors
import { HttpErrorInterceptor } from './interceptors/error.interceptor';
import { TokenInterceptor } from "./interceptors/httpconfig.interceptor";
// directive
import { KeyboardModule } from 'src/app/shared/keyboard/keyboard.module';
import { DirectivesModule } from 'src/app/shared/directives/directive.module';


@NgModule({
    declarations:[
        LoginComponent,
        RegisterComponent,
        ForgotPassComponent,
        ChangePasswordComponent,
    ],
    imports:[
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ShardModule,
        AuthRouter,
        MaterialModule,
        KeyboardModule,
        DirectivesModule
    ],
    providers: [
        // KeyboardService,
        { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpErrorInterceptor,
            multi: true
        },

    ]
})

export class AuthModule {}