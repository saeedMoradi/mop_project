import { NgModule } from "@angular/core";
import { Routes, RouterModule } from '@angular/router';
// component
import { ForgotPassComponent } from "./forgot-pass/forgot-pass.component";
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";
import { ChangePasswordComponent } from "./change-password/change-password.component";

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'forget-password', component: ForgotPassComponent},
    {path: 'change-password', component: ChangePasswordComponent},
]

@NgModule({
    imports:[RouterModule.forChild(routes)],
    exports:[RouterModule]
})

export class AuthRouter {}