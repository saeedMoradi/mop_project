import { Component, OnInit , ElementRef , ViewChild , OnDestroy , ChangeDetectorRef} from '@angular/core';
import { FormBuilder , FormGroup , Validators , FormControl } from "@angular/forms";
import { Router } from "@angular/router";
//services
import { ForgetPassService } from "../services/forget-pass.service";
import { PersistanceService } from 'src/app/core/services/persistance.service';
@Component({
  selector: 'MOP-Login-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  constructor( private forgetPassService : ForgetPassService , 
    private fb : FormBuilder,
    public router : Router,
    private ref: ChangeDetectorRef,
    private persistanceService: PersistanceService
  ) { }

  passwordForm : FormGroup;
  public notSameError = false;
  private changepassword_key ; 
  public success_msg = '';
  public codeError = '';
  public hide = true;
  public hide2 = true;
  public showNotifErorr: boolean = true;
  public step = 1;
  public error = '';
  public ErrorShortPass :boolean = false;
  // public regex =  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,15}$/;

  ngOnInit(): void {
    this.passwordForm = this.fb.group({
      password : [ '' , [
        Validators.required,
        Validators.minLength(8),
        Validators.pattern('[a-zA-Z0-9\-?\.?\!?\@?\#?\$?\%?\^?\&?\*?\(?\)?\_?\+?]*'),
      ]],
      repeatPassword : [ '' , [
        Validators.required,
        Validators.min(1),
      ]],
    })
  }

  sendPass(){
    console.log(this.ErrorShortPass);
    if(this.passwordForm.valid && !this.ErrorShortPass ) {
      this.error = '';
      if(this.password == this.repeatPassword ){
      this.notSameError = false;
     this.forgetPassService.postPass({
       "changepassword_key" : localStorage.getItem("changepassword_key") , 
       "password" : this.password.toString(), 
      }).subscribe(
      res => {
        this.showNotifErorr = !this.showNotifErorr;
        if (this.showNotifErorr == false) {
          this.codeError = '';
          this.ref.detectChanges();
        }
        console.log(res);
        this.success_msg = res['detail'];
        this.step = 2;
        this.ref.detectChanges();
      },
    ) 

    setTimeout(() => {
      if (this.showNotifErorr) {
        console.log(this.showNotifErorr, 'notif error');
        this.codeError = this.persistanceService.get('errorCode');
        console.log(this.error);
        this.ref.detectChanges();
      }
    }, 1000);

    } else{
      // this.notSameError = true;
      this.error = ' کلمه عبور انتخابی با تکرار آن یکسان نیست.';
    }
  }
    else this.error = 'کلمه عبور انتخابی از نظر امنیتی ضعیف است. در کلمه عبور انتخابی حداقل از یک حرف(بزرگ یا کوچک) و یک رقم استفاده شده باشد'
   }

  get password(){
    return this.passwordForm.get('password').value;
  }
  get repeatPassword(){
    return this.passwordForm.get('repeatPassword').value;
  }
 
  checkStrength(p) {
    // 1
    let force = 0;
  
    // 2
    const regex = /[$-/:-?{-~!"^_@`\[\]]/g;
    const letters = /[a-zA-Z]+/.test(p);
    // const upperLetters = /[A-Z]+/.test(p);
    const numbers = /[0-9]+/.test(p);
    const symbols = regex.test(p);
  
    // 3
    const flags = [letters, numbers, 
      // symbols
    ];
  
    // 4
    let passedMatches = 0;
    for (const flag of flags) {
      passedMatches += flag === true ? 1 : 0;
    }
  
    // 5
    force += 2 * p.length + ((p.length >= 10) ? 1 : 0);
    force += passedMatches * 10;
  
    // 6
    force = (p.length <= 8) ? Math.min(force, 10) : force;
  
    // 7
    force = (passedMatches === 1) ? Math.min(force, 10) : force;
    force = (passedMatches === 2) ? Math.min(force, 20) : force;
    force = (passedMatches === 3) ? Math.min(force, 30) : force; 
    if( force == 20 && symbols){
      force = 40;
    }
    if(force == 10){
      this.ErrorShortPass = true;
    }else{
      this.ErrorShortPass = false;
    }
    // console.log(force);
    return force;
  }

}
