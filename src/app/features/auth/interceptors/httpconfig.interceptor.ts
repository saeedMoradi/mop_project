import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest , HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, from, BehaviorSubject, throwError } from 'rxjs';
import { PersistanceService } from 'src/app/core/services/persistance.service'
import { AuthService } from '../services/auth.service';
import { catchError, filter, switchMap, take } from 'rxjs/operators';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  constructor(private persistanceService: PersistanceService, private authService: AuthService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>>{
    const localToken = this.persistanceService.get('access_token')
    if (localToken) {
      req = this.addToken(req, localToken)
    }
  
    return next.handle(req).pipe(catchError(err => {
      if (err instanceof HttpErrorResponse && err.status === 401) {
        return this.handle401Error(req, next)
      } else {
        return throwError(err);
      }
    }));


  }

  private isRefreshing  = false;
  private refreshTokenSubject: BehaviorSubject<any> = new BehaviorSubject<any>(null); 
  private handle401Error(req: HttpRequest<any>, next: HttpHandler) {
    if (!this.isRefreshing) {
      this.isRefreshing = true;
      this.refreshTokenSubject.next(null);

      let refreshTok = {
        'refresh_token' : this.persistanceService.get('refresh_token')
      }

      return this.authService.postRefresh(refreshTok).pipe(
        switchMap((token: any) => {
          this.isRefreshing = false;
          this.refreshTokenSubject.next(token['access_token']);
          return next.handle(this.addToken(req, token['access_token']))
        })
      )

    }else {
      return this.refreshTokenSubject.pipe(
        filter(token => token != null),
        take(1),
        switchMap(jwt => {
          return next.handle(this.addToken(req, jwt));
        }));
    }
  }


  private addToken(req: HttpRequest<any>, token: string) {
    return req.clone({
      setHeaders: {
        'Authorization': `Bearer ${token}`,
        'Accept-Language': `fa` 
      }
    });
  }

}