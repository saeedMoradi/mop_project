import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import { EMPTY, Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import { PersistanceService } from 'src/app/core/services/persistance.service';
import { AuthService } from '../services/auth.service';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private persistanceService: PersistanceService, private authService: AuthService) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      // retry(1),
      catchError((error: HttpErrorResponse) => {
        console.log('data list')
        if (error) {
          console.log(error.error.detail);
          this.persistanceService.set('errorCode', error.error.detail)
          return throwError(error)
        }else if(error.status == 401) {
          console.log('Authorization Error')
        }
      })
    );
  }
}
