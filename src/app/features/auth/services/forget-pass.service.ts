import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { 
  request_postUsername , response_postUsername,
  request_postCode ,  response_postCode ,
  request_postPass , response_postPass
} from 'src/app/shared/interfaces';
@Injectable({
  providedIn: 'root'
})
export class ForgetPassService {

  private readonly apiBaseUrl = environment.base_url

  constructor(private http: HttpClient) { }

  postUsername(data: request_postUsername): Observable<response_postUsername> {
    return this.http.post<response_postUsername>(`${this.apiBaseUrl}/login/retrieval/credential/`, data);
  }

  postCode(data: request_postCode): Observable<response_postCode> {
    return this.http.post<response_postCode>(`${this.apiBaseUrl}/login/retrieval/code/`, data);
  }

  postPass(data : request_postPass): Observable<response_postPass> {
    return this.http.post<response_postPass>(`${this.apiBaseUrl}/login/retrieval/changepassword/`, data);
  }
}
