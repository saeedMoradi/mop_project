import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { 
  response_Captcha, request_loginCredentials,
  response_loginCredentials, request_OTP, response_OTP,
  request_loginRefresh, response_loginRefresh 
} from 'src/app/shared/interfaces';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private readonly apiBaseUrl = environment.base_url

  constructor(private http: HttpClient) { }

  postCaptcha(data): Observable<response_Captcha> {
    return this.http.post<response_Captcha>(`${this.apiBaseUrl}/captcha/`, data);
  }

  postLoginCredentials(data: request_loginCredentials): Observable<response_loginCredentials> {
    return this.http.post<response_loginCredentials>(`${this.apiBaseUrl}/login/submit/credentials/`, data);
  }

  postOTP(data: request_OTP): Observable<response_OTP> {
    return this.http.post<response_OTP>(`${this.apiBaseUrl}/login/submit/otp/`, data);
  }

  postRefresh(data: request_loginRefresh): Observable<response_loginRefresh> {
    return this.http.post<response_loginRefresh>(`${this.apiBaseUrl}/login/refresh/`, data);
  }
}
