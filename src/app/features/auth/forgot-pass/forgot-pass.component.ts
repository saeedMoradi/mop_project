import { Component, OnInit , ElementRef , ViewChild , ChangeDetectorRef } from '@angular/core';
import { FormBuilder , FormGroup , Validators , FormControl } from "@angular/forms";
import { Router } from "@angular/router";
import { PersistanceService } from 'src/app/core/services/persistance.service';
//services
import { ForgetPassService } from "../services/forget-pass.service";

@Component({
  selector: 'MOP-forgot-pass',
  templateUrl: './forgot-pass.component.html',
  styleUrls: ['./forgot-pass.component.scss']
})
export class ForgotPassComponent implements OnInit {
  loginForm : FormGroup;
  codeForm : FormGroup;
  passwordForm : FormGroup;
  public error = '';
  public step = 1;
  private retrievalKey ;
  public codeError = '';

  constructor( private forgetPassService : ForgetPassService , 
    private fb : FormBuilder,
    public router : Router, 
    private ref: ChangeDetectorRef,
    private persistanceService: PersistanceService
    ) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username : new FormControl('' , [
        Validators.required,
        Validators.min(1),
      ]),
    })

    this.codeForm = this.fb.group({
      code :new FormControl( '' , [
        Validators.required,
        Validators.min(1),
      ]),
    })
  }
  sendUsername(){
  if(this.loginForm.valid){
    this.loginForm.markAllAsTouched();
  this.forgetPassService.postUsername({ "username" : this.username.value }).subscribe(res => {
      console.log(res);
      this.step = 2;
      this.retrievalKey = res['retrieval_key'];
      this.ref.detectChanges();
    },
  ) 
  }  
  }

  sendCode(){
    if(this.codeForm.valid){
      this.loginForm.markAllAsTouched();
    this.forgetPassService.postCode({
        "retrieval_key" : this.retrievalKey , 
        "retrieval_code" : this.code.value.toString(), 
      }).subscribe( res => {
        console.log(res);
        this.router.navigateByUrl('change-password');
        localStorage.setItem('changepassword_key' , res['changepassword_key'] );
      },
      ) 
    }  
    setTimeout(() => {
      this.codeError = this.persistanceService.get('errorCode');
      this.ref.detectChanges();
    }, 1000);
  }

  get username(){
    return this.loginForm.get('username');
  }
  get code(){
    return this.codeForm.get('code');
  }

}
