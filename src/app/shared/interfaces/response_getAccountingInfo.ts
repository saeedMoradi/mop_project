export interface response_getAccountingInfo{
    available: number ,
    balance: number,
    blocked: number,
    t1: number,
    t2: number
}