export interface response_getAccounting{
    [index: number] : {
        account_number: string,
        bank_name: string,
        shaba_number: string,
        status: number,
    }
}