export interface response_getIndex {
    count: number , 
    current_page: number ,
    last_page: number ,
    results: Array<object>
}