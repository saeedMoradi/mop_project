export interface response_loginRefresh {
    access_token: string;
    lifetime: number
}