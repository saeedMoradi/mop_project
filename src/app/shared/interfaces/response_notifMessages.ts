export interface response_NotifMSG {
    body: string,
    datetime: string
    id: number
    priority: boolean
    title: string
    type: number
}