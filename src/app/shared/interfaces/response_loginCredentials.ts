export interface response_loginCredentials {
    opt_type: number;
    access_token?: string;
    refresh_token?: string;
    lifetime?: number;
    opt_key?: string
}