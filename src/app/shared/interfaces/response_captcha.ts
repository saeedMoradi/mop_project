export interface response_Captcha {
    captcha_key: string;
    captcha_image: string;
    image_type: string;
    image_decode: string;
}