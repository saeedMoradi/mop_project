export interface ShowMessageData {
    title?: string,
    datetime?: string,
    body?: string,
    id: number,
    seen?: boolean,
    type: number
}