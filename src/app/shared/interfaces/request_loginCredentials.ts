export interface request_loginCredentials {
    captcha_key: string;
    captcha_value: string;
    username: string;
    password: string;
}