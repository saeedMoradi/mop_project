export interface response_getAllsurveillanceMessages{
    count: number,
    lastpage: number,
    page: number,
    results: Array<message>;
}
interface message{
    body: string , 
    datetime: string,
    id: number,
    title:string,
}
