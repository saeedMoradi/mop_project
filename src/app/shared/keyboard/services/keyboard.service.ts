import { Injectable, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { multicast } from 'rxjs/operators';

@Injectable()
export class KeyboardService {
  //   @Output() isShown: EventEmitter<any> = new EventEmitter();
  isShown: EventEmitter<any> = new EventEmitter();
  public rowKeysSet = []
  public originalRowKeysSet = [["q1[","w2]","e3{","r4}","t5#","y6%","u7^","i8*","o9+","p0="],
["a-_", "s/\\", "d:|", "f;~", "g(<", "h)>", "j$\u20ac", "k&\u00a3", "l@\u00a5"],
["z\"\";", "x..", "c,,", "v??", "b!!", "n''", "m``"],
["   "]
];

  private _shift: boolean = false;

  private _alt: boolean = false;

  private _keyboardRequested: Subject<boolean>;
  private _shiftChanged: Subject<boolean>;
  private _altChanged: Subject<boolean>;
  private _keyPressed: EventEmitter<any>;
  private _backspacePressed: Subject<void>;
  private _enterPressed: Subject<void>;
  private _randomKeys: boolean;


  constructor() {
    this._keyboardRequested = new Subject<boolean>();
    this._shiftChanged = new Subject<boolean>();
    this._altChanged = new Subject<boolean>();
    this._keyPressed = new EventEmitter<any>();
    this._backspacePressed = new Subject<void>();
    this._enterPressed = new Subject<void>();
    this.rowKeysSet = JSON.parse(JSON.stringify(this.originalRowKeysSet));
    }

  get shift(): boolean {
    return this._shift;
  }

  set shift(value:boolean) {
    this._shiftChanged.next(this._shift = value);
  }

  get alt(): boolean {
    return this._alt;
  }

  set alt(value: boolean) {
    this._altChanged.next(this._alt = value);
  }

  get keyboardRequested() {
    return this._keyboardRequested;
  }

  get shiftChanged() {
    return this._shiftChanged;
  }

  get altChanged() {
    return this._altChanged;
  }

  get keyPressed() {
    return this._keyPressed;
  }

  get backspacePressed() {
    return this._backspacePressed;
  }

  get enterPressed() {
    return this._enterPressed;
  }

  fireKeyboardRequested(show: boolean, randomKeys:boolean) {
    this._randomKeys = randomKeys;
    this.shuffleRows();
    this._keyboardRequested.next(show);
  }

  fireKeyPressed(key:string) {
    console.log(key);
    var keyboardEvent = document.createEvent('KeyboardEvent');
// var initMethod = typeof keyboardEvent.initKeyboardEvent !== 'undefined' ? 'initKeyboardEvent' : 'initKeyEvent';

keyboardEvent['initKeyboardEvent'](
  'keydown', // event type: keydown, keyup, keypress
  true, // bubbles
  true, // cancelable
  window, // view: should be window
  false, // ctrlKey
  false, // altKey
  false, // shiftKey
  false, // metaKey
  0, // keyCode: unsigned long - the virtual key code, else 0
  key.charCodeAt(0), // charCode: unsigned long - the Unicode character associated with the depressed key, else 0
);
document.dispatchEvent(keyboardEvent);
    // console.log(this._keyPressed.next(key));
    this._keyPressed.emit(key);
  }

  fireBackspacePressed() {
    this._backspacePressed.next();
  }

  fireEnterPressed() {
    this._enterPressed.next();
  }

  shuffleRows(){
    this.rowKeysSet = JSON.parse(JSON.stringify(this.originalRowKeysSet));
    if (this._randomKeys){
      for (var i=0; i < this.rowKeysSet.length; i++){
        this.shuffleArray(this.rowKeysSet[i]);
      }
    }
    this.isShown.emit(null);
  }
  shuffleArray(array) {
    var currentIndex = array.length, temporaryValue, randomIndex;
    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;
      // And swap it with the current element.
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  }
}