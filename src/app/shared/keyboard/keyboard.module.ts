import { NgModule } from "@angular/core";
import { ShardModule } from 'src/app/shared/shared.module';
import { KeyboardComponent } from './keyboard.component';
import { KeyboardService } from './services/keyboard.service';
import { DirectivesModule } from '../directives/directive.module';

@NgModule({
    declarations: [KeyboardComponent],
    imports: [ ShardModule, DirectivesModule ],
    exports: [ KeyboardComponent],
    providers: [KeyboardService]
})

export class KeyboardModule {}