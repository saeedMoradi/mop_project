import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { OskInputDirective } from './osk-input.directive';
import { KeyboardKeyDirective } from './keyboard-key.directive';

@NgModule({
    imports: [CommonModule],
    declarations: [OskInputDirective, KeyboardKeyDirective],
    exports: [OskInputDirective, KeyboardKeyDirective]
})

export class DirectivesModule {}