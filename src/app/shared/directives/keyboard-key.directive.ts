import { Directive, Input, HostBinding, HostListener, OnInit, OnDestroy } from '@angular/core';
// import * as internal from 'node:stream';
// import { subscribeOn } from 'rxjs/operators';
import { KeyboardService } from '../keyboard/services/keyboard.service';

@Directive({
  selector: '[appKeyboardKey]'
})
export class KeyboardKeyDirective implements OnInit, OnDestroy {
  private _values: string[];
  private isShifted: boolean;
  private isAlt: boolean;

  private values: string;
  private rowIndex: number;
  private keyIndex: number;

  public subscription: any;


  @Input('appKeyboardKey') indicesStr: string;

  @HostBinding('innerText') currentValue: string;

  constructor(private keyboard:KeyboardService) { this.keyboard = keyboard; }

  updateKeys()
  {
    this.values = this.keyboard.rowKeysSet[this.rowIndex][this.keyIndex];
    this._values = this.values.split('');
    this.currentValue = this._values[0];

    this.keyboard.shiftChanged.subscribe(shift => {
      this.isShifted = shift;
      this.updateCurrentValue();
    });
    this.keyboard.altChanged.subscribe(alt => {
      this.isAlt = alt;
      this.updateCurrentValue();
    });
  }

  ngOnInit() {
    this.subscription = this.keyboard.isShown.subscribe(item => this.updateKeys());

    this.rowIndex = parseInt(this.indicesStr[0]);
    this.keyIndex = parseInt(this.indicesStr[1]);
    this.updateKeys()
 }

  ngOnDestroy() {
    this.keyboard.shiftChanged.unsubscribe();
    this.keyboard.altChanged.unsubscribe();
    this.subscription.unsubscribe();
  }

  updateCurrentValue() {
    if (!this.isAlt)  {
      if (!this.isShifted) {
        this.currentValue = this._values[0];
      }
      else {
        this.currentValue = this._values[0].toUpperCase();
      }
    }
    else {
      if (!this.isShifted) {
        this.currentValue = this._values[1];
      }
      else {
        this.currentValue = this._values[2];
      }
    }
  }

  @HostListener('click')
  onClick() {
    // console.log(this.currentValue);
    this.keyboard.fireKeyPressed(this.currentValue);
  }
}