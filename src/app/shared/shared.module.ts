import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule, 
        ReactiveFormsModule,
        FormsModule,
        RouterModule, 
    ],
    exports: [
        CommonModule, 
        ReactiveFormsModule,
        RouterModule, 
    ],
})

export class ShardModule {}