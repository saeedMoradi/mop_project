import { NgModule } from "@angular/core";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AuthModule } from '../features/auth/auth.module';
// Components
import { MainLayoutComponent } from './layout/main/main-layout/main-layout.component';

@NgModule({
    declarations:[
    MainLayoutComponent,
  ],
  imports:[
    BrowserAnimationsModule,
    FormsModule, ReactiveFormsModule,
    RouterModule,
    HttpClientModule,
    AuthModule,
  ],
  exports:[MainLayoutComponent]
})

export class CoreModule {}