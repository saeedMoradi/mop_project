import {
    transition,
    trigger,
    query,
    style,
    animate,
    group,
    animateChild
  } from '@angular/animations';
  
  export const fadeInAnimation = trigger('fadeInAnimation', [
    transition('* => *', [
      query(':enter', [style({ opacity: 0 })], { optional: true }),
  
      query(
        ':enter',
        [style({ opacity: 0 }), animate('0.6s', style({ opacity: 1 }))],
        { optional: true }
      )
    ])
  ]);
  