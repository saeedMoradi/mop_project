export const environment = {
  production: true,
  base_url: 'http://broker.millenniumop.ir/api',
  captcha: '/captcha/',
  login_Credentional: '/login/submit/credentials/',
  login_OTP: '/login/submit/otp/',
  login_refresh: '/login/refresh/'
};